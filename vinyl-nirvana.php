<?php
/**
 * Plugin Name:       Vinyl Nirvana Theme Customizations
 * Description:       Customization snippets for storefront theme and woocommerce.
 * Plugin URI:        http://github.com/woothemes/theme-customisations
 * Version:           1.0.8
 * Author:            WooThemes
 * Author URI:        https://www.woocommerce.com/
 * Requires at least: 3.0.0
 * Tested up to:      4.9.5
 * WC tested up to: 	3.3.5
 * WC requires at least: 3.0.0
 *
 * @package Vinyl_Nirvana
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Main Vinyl_Nirvana Class
 *
 * @class Vinyl_Nirvana
 * @version	1.0.0
 * @since 1.0.0
 * @package	Vinyl_Nirvana
 */
final class Vinyl_Nirvana {



	/**
	 * Set up the plugin
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'vinyl_nirvana_setup' ), -1 );

		// add actions, filters, at hook plugins_loaded, this plugin's instantiation
		require_once( 'custom/functions.php' );

		// customize storefront theme
		//add_action( 'woocommerce_single_product_summary', array( $this, 'vinyl_nirvana_setup_theme' ), 1 );

		add_action( 'wp_loaded', array( $this, 'vinyl_nirvana_setup_theme' ), 40 );
		//add_action( 'wp_loaded', array( $this, 'vinyl_nirvana_setup_post' ), 40 );
		add_action( 'the_post', array( $this, 'vinyl_nirvana_setup_post' ), 40 );


	}

	/**
	 * Setup all the things
	 */
	public function vinyl_nirvana_setup() {
		add_action( 'wp_enqueue_scripts', array( $this, 'vinyl_nirvana_css' ), 999 );
		add_action( 'wp_enqueue_scripts', array( $this, 'vinyl_nirvana_js' ) );
		add_filter( 'template_include',   array( $this, 'vinyl_nirvana_template' ), 11 );
		add_filter( 'wc_get_template',    array( $this, 'vinyl_nirvana_wc_get_template' ), 11, 5 );
	}

	/**
	 * Enqueue the CSS
	 *
	 * @return void
	 */
	public function vinyl_nirvana_css() {
		wp_enqueue_style( 'custom-css', plugins_url( '/custom/style.css', __FILE__ ) );
		wp_enqueue_style( 'dashicons' );

	}

	/**
	 * Enqueue the Javascript
	 *
	 * @return void
	 */
	public function vinyl_nirvana_js() {
		wp_enqueue_script( 'custom-js', plugins_url( '/custom/custom.js', __FILE__ ), array( 'jquery' ) );
	}

	/**
	 * Look in this plugin for template files first.
	 * This works for the top level templates (IE single.php, page.php etc). However, it doesn't work for
	 * template parts yet (content.php, header.php etc).
	 *
	 * Relevant trac ticket; https://core.trac.wordpress.org/ticket/13239
	 *
	 * @param  string $template template string.
	 * @return string $template new template string.
	 */
	public function vinyl_nirvana_template( $template ) {
		if ( file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/' . basename( $template ) ) ) {
			$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/' . basename( $template );
		}

		return $template;
	}

	/**
	 * Look in this plugin for WooCommerce template overrides.
	 *
	 * For example, if you want to override woocommerce/templates/cart/cart.php, you
	 * can place the modified template in <plugindir>/custom/templates/woocommerce/cart/cart.php
	 *
	 * @param string $located is the currently located template, if any was found so far.
	 * @param string $template_name is the name of the template (ex: cart/cart.php).
	 * @return string $located is the newly located template if one was found, otherwise
	 *                         it is the previously found template.
	 */
	public function vinyl_nirvana_wc_get_template( $located, $template_name, $args, $template_path, $default_path ) {
		$plugin_template_path = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/woocommerce/' . $template_name;

		if ( file_exists( $plugin_template_path ) ) {
			$located = $plugin_template_path;
		}

		return $located;
	}

	/*
	 * Filter out value
	 */
	public function vinyl_nirvana_filter_test( $value) {
		$value = '';
		return $value;
	}

	/*
	 * Modify storefront header
	*/
	function modify_storefront_header() {
		// remove search?
		remove_action( 'storefront_header', 'storefront_product_search', 40 );
		// move cart to where search was
		remove_action( 'storefront_header', 'storefront_header_cart', 60 );
		add_action( 'storefront_header', 'storefront_header_cart', 40 );
	//    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

		//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

	//remove_action( 'woocommerce_single_product_summary', 'WC_Full_Page_Add_To_Cart::wc_fw_template_single_add_to_cart', 30 );
	remove_action( 'storefront_header', 'storefront_primary_navigation', 50 );
	add_action( 'storefront_header', 'vn_storefront_primary_navigation', 50 );

	//remove_action( 'storefront_header', 'storefront_site_branding', 20 );
	//add_action( 'storefront_header', 'storefront_site_branding', 5 );

	}

	function modify_component_test(  $product, $component_id, $composite ) {

		echo '<div class="learn-more"><a href="' . esc_url( get_permalink( $product->id)) . '" target="_blank"> Learn more ...</a> <br><br></div>';

	}

	// remove background images in most cases
	public function  disable_custom_background_css( $classes) {
		global $post;
		$flag = true;

		// keep background for CYT page or product
		if ( $post ) {
			if ( is_product() ) {
					$terms = wp_get_post_terms( $post->ID, 'product_cat' );
					$list = array();
					foreach ( $terms as $term ) $list[] = $term->slug;
					if ( count( $list) && in_array('diy-mod-project', $list) )  {
						$flag = false;
					}
				}
			if ( $post->ID == 126)
					$flag = false;
		}

		// remove CSS for background
		if ( $flag )  {
			$key = array_search('custom-background', $classes, true);
			if( $key !== false )
				unset( $classes[$key] );
		}

		return $classes;
	}

	public function vinyl_nirvana_setup_theme() {

		//return;
		//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

		// single product: this action by 3rd party plugin removes add-to cart
		//remove_action( 'woocommerce_before_single_product', 'WC_Full_Page_Add_To_Cart::wc_fw_single_add_to_cart', 10 );

		// all pages
		add_action( 'storefront_before_site', array( $this, 'modify_storefront_header'));

		// this works if called at start this hook, priority 5
		//remove_action( 'woocommerce_single_product_summary', 'WC_Full_Page_Add_To_Cart::wc_fw_template_single_add_to_cart', 30 );

		// this is done by another plugin
		//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

		//add_action( 'woocommerce_composite_component_options_thumbnails', array( $this, 'modify_component_thumbnails'), 12, 2 );
		add_action( 'woocommerce_composited_product_details', array( $this,'modify_component_test'), 12, 3 );

		// add google font here
		wp_enqueue_style( 'google-font-'.'crimson-text', "https://fonts.googleapis.com/css?family=Crimson+Text:400,700i", array(), null);

		// filter background
		//add_filter('body_class', array( $this, 'disable_custom_background_css'));

		// disable woocoomerce product zoom
		remove_theme_support( 'wc-product-gallery-zoom' ); // does anything?
		add_filter( 'woocommerce_single_product_photoswipe_options', function( $options ) {
			$options['zoomEl'] = 'false';
			$options['maxSpreadZoom'] = '1';
			$options['getDoubleTapZoom'] = 'function(isMouseClick, item) {
					return item.initialZoomLevel; }';
			$options['pinchToClose'] = 'false';
			return $options;
		}, 10 );

	}

	public function vinyl_nirvana_setup_post() {
		global $post;

		// return;

		// remove add to cart button on kit products
		if( is_product())
		 if( $post ) {

			 // get all kit parts
			 $args = array(
				 'post_type' => 'product',
				 'posts_per_page' => -1,
				 'tax_query' => array(
					 		// 'relation' => 'OR',
							array(
								'taxonomy' => 'product_cat',
								'field'    => 'slug',
								'terms'    => 'kit-parts',
								// 'terms'    => 'td-160-plinth',
							),
						),
			 );
			 $kit_parts = get_posts( $args);
			 // build list of IDs
			 $list = array();
			 foreach ( $kit_parts as $part ) $list[] = $part->ID;

			 // $intersect = array_intersect( $list, $hide_list);
			 // if ( count( $list) && in_array('kit-parts', $list) )  {
			 //if ( count( $intersect) )  {
			 if ( count( $list) && in_array( $post->ID, $list) )  {

				 // single product: this action by 3rd party plugin removes add-to cart
				 remove_action( 'woocommerce_before_single_product', 'WC_Full_Page_Add_To_Cart::wc_fw_single_add_to_cart', 10 );
				 remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
				 remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

				 remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

			 }

		}

	}

} // End Class

/**
 * The 'main' function
 *
 * @return void
 */
function vinyl_nirvana_main() {
	new Vinyl_Nirvana();
}

/**
 * Initialise the plugin
 */
add_action( 'plugins_loaded', 'vinyl_nirvana_main' );
