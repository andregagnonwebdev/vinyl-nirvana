<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */

 //////////////////////////////////////////////////////////////
 //
 // WooCommerce
 //

 add_filter( 'get_terms', 'vn_get_subcategory_terms', 10, 3 );

function vn_get_subcategory_terms( $terms, $taxonomies, $args ) {

  $new_terms = array();

  // if a product category and on the shop page
	if ( in_array( 'product_cat', $taxonomies ) && ! is_admin() ) {
  //if ( in_array( 'product_cat', $taxonomies ) && ! is_admin() && is_shop() ) {

    foreach ( $terms as $key => $term ) {
			if ( 0 || is_object( $term)) {

				$slug = $term->slug;
				$hide_list = array(
					'kit-parts', 'td-160-plinth', 'td-160-top-plate', 'td-160-tonearm',
					 'td-125-plinth', 'td-150-plinth', 'td-150-tonearm',
					 'td-160',
					 'accessories-diy-mod-project',
					'thorens-part-td-165',
					'thorens-parts', //old term
					'turntable-services',
					'uncategorized',
					'manufacturer', 'accessories',	'diy-mod-project' );

				//if ( 1) {
	      if ( !in_array( $slug, $hide_list ) ) {
	        $new_terms[] = $term;
	      }
			}
			else {
				$new_terms[] = $term;

			}

    }

    $terms = $new_terms;
  }

  return $terms;
}

 // admin: remove woocommerce nag message
 add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );

 // remove storefront theme credit
 add_filter( 'storefront_credit_link',  array( $this, 'vinyl_nirvana_filter_test' ));
 //add_filter( 'storefront_credit_link',  '__return_""' );

 // adjust storefront theme header
 //add_action( 'storefront_before_site', array( $this, 'modify_storefront_header'));

 global $product;


 //remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
 //remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

 remove_action( 'woocommerce_single_product_summary', 'WC_Full_Page_Add_To_Cart::wc_fw_template_single_add_to_cart', 30 );

 // add product search to sidebar
 add_action( 'storefront_before_site', 'vns_sidebar_stuff', 10 );
 function vns_sidebar_stuff() {
   //add_action( 'dynamic_sidebar_after', 'storefront_product_search', 40 );
 }
 //add_filter( 'woocommerce_composited_product_image_html', 'vns_filter_cpi', 10);

 function vns_filter_cpi(  $html ) {
	 return 'hey';
 }

 function vn_filter_product_image(  $html ) {
	 global $product;
	 $s = $html;
	 if ( 0 && is_composite_product()) {
		 $s = ''; //'here '; //print_r( $product, true);
		 return $s;

			$components = $product->get_components();
			if ( $components) {
				foreach ( $components as $component_id => $component ) {
					$s .= $component->get_title( true ) .'<br>';
				}
			}
			 	//$s .= print_r( $components, true);

		 // check each cart item for our category
		 foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

		     $product = $cart_item['data'];

				 $s .= $cart_item['product_id'] . ' ';
				 //$s .= print_r( $product, true);
				 if ( wc_cp_is_composite_container_cart_item( $cart_item)) {
					 //$s .= print_r( $cart_item, true);

					 $s .= '-container ';
				 }
			 }

		}

		return $s;
	}

 add_filter( 'woocommerce_single_product_image_thumbnail_html', 'vn_filter_product_image', 10);

// add weight to admin column
 // Store cart weight in the database
 add_action('woocommerce_checkout_update_order_meta', 'vn_woo_add_cart_weight');

 function vn_woo_add_cart_weight( $order_id ) {
     global $woocommerce;

     $weight = $woocommerce->cart->cart_contents_weight;
     update_post_meta( $order_id, '_cart_weight', $weight );
 }

 // Add order new column in administration
 add_filter( 'manage_edit-shop_order_columns', 'vn_woo_order_weight_column', 20 );

 function vn_woo_order_weight_column( $columns ) {

	 $offset = 8;
 	$offset = 6;
 	$updated_columns = array_slice( $columns, 0, $offset, true) +
 	array( 'total_weight' => esc_html__( 'Weight', 'woocommerce' ) ) +
 	array_slice($columns, $offset, NULL, true);

 	return $updated_columns;
 }

 // Populate weight column
 add_action( 'manage_shop_order_posts_custom_column', 'vn_woo_custom_order_weight_column', 2 );

 function vn_woo_custom_order_weight_column( $column ) {
 	global $post;

 	if ( $column == 'total_weight' ) {
 		$weight = get_post_meta( $post->ID, '_cart_weight', true );
 		if ( $weight > 0 )
 			print $weight . ' ' . esc_attr( get_option('woocommerce_weight_unit' ) );
 		else print 'N/A';
 	}
 }



// Display total weight on the cart and checkout pages
add_action( 'woocommerce_cart_totals_before_order_total', 'vn_add_weight_to_cart' );
add_action( 'woocommerce_review_order_before_order_total', 'vn_add_weight_to_cart' );
function vn_add_weight_to_cart() {
    if ( WC()->cart->needs_shipping() ) { ?>
        <tr class="weight">
            <th><?php _e( 'Total Weight', 'woocommerce-cart-weight' ); ?></th>
            <td><?php echo WC()->cart->cart_contents_weight . ' ' . get_option( 'woocommerce_weight_unit' ); ?></td>
        </tr>
    <?php }
}

// To use this snippet, download this file into your plugins directory and activate it, or copy the code under this line into the functions.php file of your (child) theme.
add_filter( 'woocommerce_component_options_hide_incompatible', 'vn_wc_cp_component_options_hide_incompatible', 10, 3 );
function vn_wc_cp_component_options_hide_incompatible( $hide, $component_id, $composite ) {
	return true;
}

if ( ! function_exists( 'vn_storefront_primary_navigation' ) ) {
	/**
	 * Display Primary Navigation
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function vn_storefront_primary_navigation() {
		?>
		<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
		<button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( '&nbsp;', 'storefront' ) ) ); ?></span></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location'	=> 'primary',
					'container_class'	=> 'primary-navigation',
					)
			);

			wp_nav_menu(
				array(
					'theme_location'	=> 'handheld',
					'container_class'	=> 'handheld-navigation',
					)
			);
			?>
		</nav><!-- #site-navigation -->
		<?php
	}
}

function vn_sc_list_child_pages() {

global $post;

if ( is_page() && $post->post_parent )

    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
else
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );

if ( $childpages ) {

    $string = '<ul class="vn-child-pages">' . $childpages . '</ul>';
}

return $string;

}

add_shortcode('vn_sc_childpages', 'vn_sc_list_child_pages');

/* AdSense */
add_action( 'storefront_page_before', 'vn_test_adsense', 10);
add_action( 'storefront_loop_before', 'vn_test_adsense', 10);
add_action( 'storefront_single_post_before', 'vn_test_adsense', 10);
add_action( 'storefront_page_after', 'vn_test_adsense', 10);
add_action( 'storefront_loop_after', 'vn_test_adsense', 10);
add_action( 'storefront_single_post_after', 'vn_test_adsense', 10);

function vn_test_adsense( $value='') {
	if ( is_page( 'home')
		|| is_shop()
		|| is_cart()
		|| is_checkout()
		|| is_page( 'vintage-turntables-for-sale')
		|| is_page( 'customize-your-thorens')
		|| is_page( 'restoration_services')
		|| is_page( 'turntable-consultation-page')
		)
		return;

	if ( 0)
		echo '<div style="background-color:#fafafa;margin: 0 0 20px;padding: 20px;"><b>Adsense</b><br /></div>';
	else {
		echo '<div style="margin: 0 0 30px;">';
		echo '<ins class="adsbygoogle"   style="display:block;"   data-ad-client="ca-pub-6371778426592194"   data-ad-slot="5910302288"   data-ad-format="auto"></ins>';
		echo '<script>	(adsbygoogle = window.adsbygoogle || []).push({});	</script>';
		echo '</div>';
	}

}
// turn off warning, for smart redirect plugin
error_reporting(E_ALL ^ E_WARNING);



//add_filter('woocommerce_default_catalog_orderby', 'vn_custom_default_catalog_orderby');
// function vn_custom_default_catalog_orderby() {
//      return 'menu_order'; // Can also use title and price
// }


// To use this snippet, download this file into your plugins directory and activate it, or copy the code under this line into the functions.php file of your (child) theme.
add_filter( 'woocommerce_composite_component_default_orderby', 'vn_wc_cp_sort_by_menu_order', 10, 3 );
function vn_wc_cp_sort_by_menu_order( $default_sort_function, $component_id, $composite ) {
	return 'menu_order';
}

// does anything here? see setup_theme
remove_theme_support( 'wc-product-gallery-zoom' );
add_filter( 'woocommerce_single_product_photoswipe_options', function( $options ) {
	$options['zoomEl'] = 'false';
	$options['maxSpreadZoom'] = '1';
	return $options;
}, 10 );


///////////////////////
 // test
 add_action( 'loop_start', 'vn_remove_theme_stuff', 0 );
 function vn_remove_theme_stuff() {
 //    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
 }

 // test
 add_action( 'storefront_sidebar', 'vn_test_action', 10);
 function vn_test_action( $value='') {
   // if ( is_woocommerce() )
   //   echo '<b>Shopping</b>';
   global $post;
   //var_dump( $post);
 }

 // test
 //add_action( 'storefront_page_before', 'vn_test_action_2', 10);
 //add_action( 'storefront_page_after', 'vn_test_action_2', 10);
 function vn_test_action_2( $value='') {
   echo '<b>TEST</b><br />';
 }
